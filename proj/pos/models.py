# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class Slot(models.Model):
    reference_id = models.UUIDField()
    dock_id = models.IntegerField()
    slot_start_dt = models.DateTimeField()
    slot_end_dt = models.DateTimeField()
    capacity = models.IntegerField()

    class Meta:
        unique_together = (
            ('reference_id', 'dock_id', 'slot_start_dt', 'slot_end_dt')
        )

    def __unicode__(self):
        return "{} {} {}".format(
            self.reference_id, self.dock_id. self.capacity
        )

    @property
    def desc(self):
        return "dock_id %s slot from %s to %s of capacity %s" % (
            self.dock_id, self.slot_start_dt, self.slot_end_dt, self.capacity
        )


class PO(models.Model):
    reference_id = models.UUIDField()
    po_id = models.IntegerField()
    item_id = models.IntegerField()
    quantity = models.IntegerField()

    class Meta:
        unique_together = (
            ('reference_id', 'po_id', 'item_id')
        )

    def __unicode__(self):
        return "{} {} {} {}".format(
            self.reference_id, self.po_id, self.item_id, self.quantity
        )

    @property
    def desc(self):
        return "po_id %s item_id %s with quantity %s" % (
            self.po_id, self.item_id, self.quantity
        )


class Schedule(models.Model):
    reference_id = models.UUIDField()
    po_obj = models.ForeignKey(PO)
    slot_obj = models.ForeignKey(Slot)

    class Meta:
        unique_together = (
            ('reference_id', 'po_obj', 'slot_obj')
        )

    def __unicode__(self):
        return "{} po:{} slot{}".format(
            self.reference_id, self.po_obj, self.slot_obj
        )

    @property
    def desc(self):
        return "%s scheduled_at %s" % (
            self.po_obj.desc, self.slot_obj.desc
        )
