import os
import csv

from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from pos.service import compute_optimum_schedule


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            '--po', dest='po_path',
            action='store',
            help="""file containing PO data.
            This file should reside in data folder"""
        )
        parser.add_argument(
            '--slot', dest='slot_path',
            action='store',
            help="""file containing slot-dock data.
            This file should reside in data folder"""
        )

    def handle(self, *args, **options):
        if not options['po_path'] or not options['slot_path']:
            raise CommandError('You should pass both PO and slot file')

        po_path = os.path.join(
            settings.SETTINGS_PATH,
            '../', 'data/{}'.format(options['po_path'])
        )
        slot_path = os.path.join(
            settings.SETTINGS_PATH,
            '../', 'data/{}'.format(options['slot_path'])
        )

        po_data = []
        with open(po_path) as f:
            po_data = list(
                csv.DictReader(f)
            )

        slot_data = []
        with open(slot_path) as f:
            slot_data = list(
                csv.DictReader(f)
            )

        df = compute_optimum_schedule(po_data, slot_data)

        self.stdout.write(
            self.style.SUCCESS(
                df.to_string()
            )
        )
