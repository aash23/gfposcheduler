import os

from django.conf import settings
from django.core.management.base import BaseCommand, CommandError
from pos.service import store_data_and_compute_schedule


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            '--po', dest='po_path',
            action='store',
            help="""file containing PO data.
            This file should reside in data folder"""
        )
        parser.add_argument(
            '--slot', dest='slot_path',
            action='store',
            help="""file containing slot-dock data.
            This file should reside in data folder"""
        )

    def handle(self, *args, **options):
        if not options['po_path'] or not options['slot_path']:
            raise CommandError('You should pass both PO and slot file')

        po_path = os.path.join(
            settings.SETTINGS_PATH,
            '../', 'data/{}'.format(options['po_path'])
        )
        slot_path = os.path.join(
            settings.SETTINGS_PATH,
            '../', 'data/{}'.format(options['slot_path'])
        )

        reference_id = store_data_and_compute_schedule(po_path, slot_path)

        if reference_id:
            self.stdout.write(
                self.style.SUCCESS(
                    """Successfully created the schedule
                    for the slot-data and PO provided.
                    reference_id for search purposes is %s
                    """ % (reference_id,)
                )
            )
