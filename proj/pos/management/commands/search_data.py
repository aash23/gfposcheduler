import datetime
from django.core.management.base import BaseCommand, CommandError
from pos.models import Slot, Schedule


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            'reference_id',
            nargs='?'
        )
        parser.add_argument(
            '--dock_id', dest='dock_id',
            action='store',
            help="""Search by dock_id"""
        )
        parser.add_argument(
            '--date', dest='date',
            action='store',
            help="""seach by date. It should be in YYYY-mm-dd format"""
        )

    def handle(self, *args, **options):
        if not options['dock_id'] and not options['date']:
            raise CommandError('You should either pass dock_id or date')

        if options['dock_id']:
            slots = Slot.objects.filter(
                dock_id=options['dock_id'],
                reference_id=options['reference_id']
            )

            for slot in slots:
                self.stdout.write(
                    self.style.SUCCESS(
                        "slot details are: %s" % slot.desc
                    )
                )

                if slot.schedule_set.exists():
                    for schedule in slot.schedule_set.all():
                        self.stdout.write(
                            self.style.SUCCESS(
                                "\t %s" % (schedule.po_obj.desc)
                            )
                        )
                else:
                    self.stdout.write(
                        self.style.SUCCESS(
                            "No Schedule"
                        )
                    )
        elif options['date']:
            d = datetime.datetime.strptime(options['date'], '%Y-%m-%d').date()
            schedules = Schedule.objects.filter(
                slot_obj__slot_start_dt__date__gte=d,
                slot_obj__slot_end_dt__date__lte=d
            )
            self.stdout.write(
                self.style.SUCCESS(
                    "Schedule for %s is:" % (options['date'],)
                )
            )
            for schedule in schedules:
                self.stdout.write(
                    self.style.SUCCESS(schedule.desc)
                )
