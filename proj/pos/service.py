import pandas as p
import csv
import uuid
from .models import Slot, PO, Schedule


def compute_optimum_schedule(pos_data, slot_data):
    """
        1. Find slot with the exact capacity as that of PO. If found, use that
        2. If found, reduce the dock's capacity to zero
        3. Else, find slot with the capacity that is just greater than
            that of PO.
        4. If found, update the dock's capacity with the remaining capacity
        5. Make a new result_df that will contain the schedule for the day
    """
    po_headers = pos_data[0].keys()
    sd_headers = slot_data[0].keys()
    result_headers = sd_headers + po_headers
    result_headers.remove('capacity')

    po_data = []
    for dt in pos_data:
        po_data.append(
            [int(x) for x in dt.values()]
        )

    sd_data = []
    for dt in slot_data:
        r = []
        for key, value in dt.iteritems():
            if key in ['dock_id', 'capacity']:
                r.append(int(value))
            else:
                r.append(value)
        sd_data.append(r)

    result_data = []

    po_df = p.DataFrame(data=po_data, columns=po_headers)
    sd_df = p.DataFrame(data=sd_data, columns=sd_headers)

    for _i, row in po_df.iterrows():
        matching_df = sd_df[(sd_df.capacity == row.quantity)]
        if not matching_df.empty:
            sd_row = matching_df.iloc[0]

            result_data.append(
                list(sd_row.values)[:-1] + list(row.values)
            )
            sd_df.capacity.iloc[sd_row.name] = 0
        else:
            closest_matching_df = sd_df[
                (sd_df.capacity >= row.quantity)
            ].sort_values(['capacity', 'slot_start_dt'], ascending=[True, True])

            if not closest_matching_df.empty:
                sd_row = closest_matching_df.iloc[0]

                result_data.append(
                    list(sd_row.values)[:-1] + list(row.values)
                )
                new_capacity = sd_row.capacity - row.quantity
                sd_df.capacity.iloc[sd_row.name] = new_capacity

    return p.DataFrame(data=result_data, columns=result_headers)


def store_data_and_compute_schedule(po_path, slot_path):
    """
    This function stores PO data in PO table, slot data in slot table.
    It then computes the optimum schdule, and stores the returned schedule
    in another table called `Schedule`
    """

    reference_id = uuid.uuid1()

    po_data = []
    po_objs = []
    with open(po_path) as f:
        for row in csv.DictReader(f):
            po_data.append(row)
            po_objs.append(
                PO(
                    reference_id=reference_id, po_id=row['po_id'],
                    item_id=row['item_id'], quantity=row['quantity']
                )
            )
    PO.objects.bulk_create(
        po_objs
    )

    slot_data = []
    slot_objs = []
    with open(slot_path) as f:
        for row in csv.DictReader(f):
            slot_data.append(row)
            slot_objs.append(
                Slot(
                    reference_id=reference_id, dock_id=row['dock_id'],
                    slot_start_dt=row['slot_start_dt'],
                    slot_end_dt=row['slot_end_dt'],
                    capacity=row['capacity']
                )
            )
    Slot.objects.bulk_create(
        slot_objs
    )

    df = compute_optimum_schedule(po_data, slot_data)
    for index, row in df.iterrows():
        po_id = row['po_id']
        dock_id = row['dock_id']
        item_id = row['item_id']
        slot_start_dt = row['slot_start_dt']
        slot_end_dt = row['slot_end_dt']

        slot_obj = Slot.objects.get(
            reference_id=reference_id,
            dock_id=dock_id,
            slot_start_dt=slot_start_dt,
            slot_end_dt=slot_end_dt
        )
        po_obj = PO.objects.get(
            reference_id=reference_id,
            po_id=po_id,
            item_id=item_id
        )

        Schedule.objects.create(
            reference_id=reference_id,
            po_obj=po_obj,
            slot_obj=slot_obj
        )

    return reference_id
