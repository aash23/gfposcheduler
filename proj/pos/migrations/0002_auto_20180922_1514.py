# -*- coding: utf-8 -*-
# Generated by Django 1.11.15 on 2018-09-22 15:14
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pos', '0001_initial'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='po',
            unique_together=set([('reference_id', 'po_id', 'item_id')]),
        ),
    ]
