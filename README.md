# gfposcheduler

## Project Requirements

1. This project requires python 2.7, pip and virtualenv to be installed

## Steps to setup the project
1. After getting the source code, go inside the folder and setup virtual environment by running:

`virtualenv gfenv`

2. Activate the environment by running:

`source gfenv/bin/activate`

3. Install Requirements by running:

`pip install -r requrements.txt`

4. Run migrations by executing:

`cd proj && ./manage.py migrate`

Now our application is ready

## Sample data location
Sample data is located in data folder. This data will be used later in the section to compute optimum schedules. The computation commands that you will see later will assume that the files are located inside the data folder. So you only need to pass the filename to the script to start the computation
## Commandline tools
There are three management commands written in this project
1. Computing the optimal schedule given the PO and slot-dock data
2. Store the PO and slot-dock data if it is to be used for search purposes later
3. Search through the stored data using the reference_id returned from the previous task
#### 1. Compute Optimum Schedule
This task computes the optimal schedule using the following algorithm:
1. Find slot with the exact capacity as that of PO. If found, use that
2. If found, reduce the dock's capacity to zero
3. Else, find slot with the capacity that is just greater than
    that of PO.
4. If found, update the dock's capacity with the remaining capacity
5. Make a new result_df that will contain the schedule for the day

The command to run is:
`./manage.py optimum_schedule --po pos-test.csv --slot slots-test.csv`
#### 2. Store PO and Slot Data
There are three models/tables used to store the data - `Slot`, `PO` and `Schedule`. `Slot` and `PO` will contain the data that was present in the files passed. `Schedule` will contain the optimum schedules based on the data.

Note:
It is mandatory to pass both slot and PO data will storing it. This is needed as we would be calculating the optimum schedules and storing it, so that it can be used for searching later. Because of this a term named `reference_id` is introduced to refer to the instance when the store operation is performed. It ensures that slot and PO data of different store operations do not mix with each other.

To perform search operation on the data stored, it is mandatory to pass the `reference_id`. So, do make sure that you copy the `reference_id` that you will receive in the output of this command
#### 3. Search by dock_id or date in the data stored
The command to run is:
` ./manage.py search_data <reference_id> --dock_id=1`

OR

`./manage.py search_data <reference_id> --date=2018-08-01`

`<reference_id>` is the id that you will receive while storing the data (See Step 2)
